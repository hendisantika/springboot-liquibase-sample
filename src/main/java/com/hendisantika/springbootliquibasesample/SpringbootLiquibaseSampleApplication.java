package com.hendisantika.springbootliquibasesample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLiquibaseSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLiquibaseSampleApplication.class, args);
    }

}
