package com.hendisantika.springbootliquibasesample.repository;

import com.hendisantika.springbootliquibasesample.domain.Athlete;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-liquibase-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-08-07
 * Time: 18:20
 */
interface AthleteRepository extends CrudRepository<Athlete, Long> {
    List<Athlete> findByFirstNameIgnoreCase(@Param("firstName") String firstName);
}