--liquibase formatted sql
--changeset Hendi Santika:001.2
INSERT INTO athlete(id, first_name, last_name, country) VALUES (1, 'Uzumaki', 'Naruto', 'Konohagakure');
INSERT INTO athlete(id, first_name, last_name, country) VALUES (2, 'Uchiha', 'Sasuke', 'Konohagakure');
INSERT INTO athlete(id, first_name, last_name, country) VALUES (3, 'Sakura', 'Haruno', 'Konohagakure');