--liquibase formatted sql
--changeset Hendi Santika:001.1
CREATE table athlete(
id serial PRIMARY KEY,
first_name VARCHAR(128) DEFAULT 'NULL' NULL,
last_name VARCHAR(128) DEFAULT 'NULL' NULL,
country VARCHAR(256) DEFAULT 'NULL' NULL);