# Spring Boot Liquibase Sample

This is only little sample on how is Liquibase is using in Spring Boot.

Actually, I am prefer to use [FlywayDB](https://flywaydb.org/) than [Liquibase](https://www.liquibase.org/).

FlywayDB its easier to use than Liquibase.

If You want to see my repository on using FlywayDB, please check this link [here](https://gitlab.com/hendisantika/springboot-flyway-postgres-docker?nav_source=navbar) and [here](https://gitlab.com/hendisantika/spring-boot-flyway?nav_source=navbar).

## Things to do:
I assume that You already installed JDK8, maven and PostgreSQL on yuo machine.

1. Clone the repository: `git clone git@gitlab.com:hendisantika/springboot-liquibase-sample.git`.
2. Go to the folder: `cd springboot-liquibase-sample`.
3. Run the app: `mvn clean spring-boot:run`.

